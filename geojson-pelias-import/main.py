import argparse
from dataclasses import asdict

from elasticsearch import helpers, Elasticsearch

from pelias_schema import Doc

import ijson


def import_data(file, es_host, es_port, es_name, es_pass, es_schema):
    es = Elasticsearch(hosts=[f"{es_host}:{es_port}"],
                       basic_auth=(es_name, es_pass))

    # es = Elasticsearch(
    #     ['https://development.es.europe-west4.gcp.elastic-cloud.com:9243'],
    #     basic_auth=('elastic', 'iSNrGN2SfDdXTecmTYagay6T'))

    f = open(file, 'r')
    features = (feature for feature in ijson.items(f, 'features.item', use_float=True)
                if feature['type'] == 'Feature')
    total = 0
    actions = []
    for feature in features:
        if feature["geometry"]["type"] == "Point":
            actions.append({
                "_index": es_schema,
                "_source": asdict(Doc(feature, "here_dataset"))
            })
            total += 1
            if total % 1000 == 0:
                print(f"inserted {total}")
                helpers.bulk(es, actions, request_timeout=60)
                actions = []
    helpers.bulk(es, actions, request_timeout=60)
    f.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_file', required=True, type=str, help='input_file')
    parser.add_argument('--es_host', required=True, type=str, help='host')
    parser.add_argument('--es_port', required=True, type=str, help='port')
    parser.add_argument('--es_user', required=True, type=str, help='user')
    parser.add_argument('--es_pass', required=True, type=str, help='pass')
    parser.add_argument('--es_schema', required=True, type=str, help='schema')
    args = parser.parse_args()
    import_data(args.input_file,
                args.es_host,
                args.es_port,
                args.es_host,
                args.es_pass,
                args.es_schema)
