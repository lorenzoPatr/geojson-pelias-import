from dataclasses import dataclass


@dataclass
class Coords:
    lat: float
    lon: float

@dataclass
class Name:
    default: str

@dataclass
class AddressParts:
    name: str = ""

@dataclass
class Phrase:
    country: str = ""

@dataclass
class Parent:
    country: str = ""

@dataclass
class Dummy:
    properties: str = ""


@dataclass
class Doc:
    name: Name = Name("")
    phrase: Phrase = Phrase()
    parent: Parent = Parent()
    address_parts: AddressParts = AddressParts()
    center_point: Coords = Coords(0, 0)
    category: str = ""
    source: str = ""
    layer: str = ""
    source_id: str = ""
    bounding_box: str = ""
    popularity: str = ""
    population: str = ""
    addendum: Dummy = Dummy()


    def __init__(self, feature, source):
        self.name = Name(feature["properties"]["POIName"])
        # self.name = Name(feature["properties"]["poi_name"])
        self.address_parts = AddressParts(f'Indonesia')
        self.category = feature["properties"]["CategoryName"]
        self.source = source
        self.center_point = Coords(lat=feature["geometry"]["coordinates"][1], lon=feature["geometry"]["coordinates"][0])
        self.parent = Parent("ID")


